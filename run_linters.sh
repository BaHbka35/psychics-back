#!/bin/bash
isort psychics/
echo +++++++++++++++++++++++++++
autoflake -r --config ./setup.cfg psychics/
echo +++++++++++++++++++++++++++
mypy psychics/
echo +++++++++++++++++++++++++++
flake8 psychics/
echo +++++++++++++++++++++++++++
