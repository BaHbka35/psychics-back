# Тестовое задания для 'EDISON Software Development Centre'

[Само тз](implementation_specification.md)


Для запуска необходимо создать .env файл, содержимое которого показанно ниже.

Затем необходимо запустить команду `sudo docker compose up --build`

.env
```bazaar
DEBUG=True

DB_NAME=name
DB_USER=user
DB_PASSWORD=password
DB_HOST=db
DB_PORT=5432
```

Схема бд

![Схема БД](db_scheme.png)


Экстрасенсы создаются при старте приложения. Создается трое экстрасенсов.