FROM python:3.10.7-slim as first

RUN apt update \
    && apt install -y curl python3-dev

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH=/root/.local/bin/:${PATH}

COPY poetry.lock pyproject.toml ./

# Install dependencies in system, not in virtual environment
RUN poetry config virtualenvs.create false \
    && poetry check && poetry install --no-root --only main

from python:3.10.7-alpine as second

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY --from=first /usr/local/lib/python3.10/site-packages /usr/local/lib/python3.10/site-packages

WORKDIR /code

COPY . .

ENTRYPOINT ["/bin/sh", "./entrypoint.sh"]
