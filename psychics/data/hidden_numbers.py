from psychics.game.dtos.hidden_numbers import UserHiddenNumber

round_id_type = str

hidden_numbers: dict[round_id_type, UserHiddenNumber] = {}