from psychics.psychics.dtos import Psychic

psychic_id_type = str

psychics: dict[psychic_id_type, Psychic] = {}