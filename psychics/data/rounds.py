from psychics.game.dtos.round import Round

user_session_id_type = str

rounds: dict[user_session_id_type, list[Round]] = {}