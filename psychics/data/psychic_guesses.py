from psychics.game.dtos.psychic_guess import PsychicGuess

psychic_id_type = str
round_id_type = str


psychics_guesses_by_round: dict[round_id_type, list[PsychicGuess]] = {}
psychics_guesses_by_psychic_id: dict[psychic_id_type, list[PsychicGuess]] = {}
