from enum import Enum


class DBTables(str, Enum):
    users: str = 'users'
    psychics: str = 'psychics'
    rounds: str = 'rounds'
    psychic_guesses: str = 'psychic_guesses'
    user_hidden_numbers: str = 'user_hidden_numbers'
