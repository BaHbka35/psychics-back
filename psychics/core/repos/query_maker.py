from psychics.core.enums import DBTables


class BaseSQLQueryMaker:

    def get_by_pk(self, table: DBTables) -> str:
        query: str = f'SELECT * FROM {table} WHERE id=$1'
        return query

    def get_by_where(
            self,
            *,
            table: DBTables,
            where: str,
    ) -> str:
        query: str = f'SELECT * FROM {table} WHERE {where}'
        return query

    def get_list(
            self,
            *,
            table: DBTables,
            where: str = '',
            limit: int | None = None,
            offset: int | None = None,
            order_by: tuple[str, ...] = tuple(),
    ) -> str:
        where_part: str = f'WHERE {where}' if where else ''
        limit_part: str = f'LIMIT {limit}' if limit else ''
        offset_part: str = f'OFFSET {offset}' if offset else ''
        order_by_part: str = ''
        if order_by:
            order_by_part = 'ORDER BY ' + ", ".join(
                [f"{i[1:]} DESC" if i.startswith("-") else i for i in order_by])
        query: str = (
            f'SELECT * FROM {table} '
            f'{where_part} {order_by_part} {offset_part} {limit_part}'
        )
        return query

    def get_total(
            self,
            *,
            table: DBTables,
            where: str = '',
    ) -> str:
        where_part: str = f'WHERE {where}' if where else ''
        query: str = (
            f'SELECT count(*) AS total '
            f'FROM {table} {where_part}'
        )
        return query

    def create(
            self,
            *,
            table: DBTables,
            data_keys: tuple[str | int, ...]
    ) -> str:
        fields: list[str] = []
        placeholders: list[str] = []
        for idx, key in enumerate(data_keys):
            fields.append(str(key))
            placeholders.append(f'${idx + 1}')
        query: str = (
            f'INSERT INTO {table} '
            f'({", ".join(fields)}) '
            f'VALUES ({", ".join(placeholders)}) '
            f'RETURNING *'
        )
        return query

    def update_by_pk(
            self,
            *,
            table: DBTables,
            data_keys: tuple[str | int, ...]
    ) -> str:
        statements: list[str] = []
        for idx, key in enumerate(data_keys):
            statement = f"{key} = ${idx + 1}"
            statements.append(statement)
        ready_set_str: str = ', '.join(statements)
        query: str = (
            f'UPDATE {table} '
            f'SET {ready_set_str} '
            f'WHERE id = ${len(data_keys) + 1} '
            f'RETURNING *'
        )
        return query

    def delete_by_pk(self, table: DBTables) -> str:
        query: str = f'DELETE FROM {table} WHERE id=$1'
        return query
