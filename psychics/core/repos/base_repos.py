from asyncpg import Connection
from asyncpg.exceptions import UniqueViolationError

from psychics.core.enums import DBTables
from psychics.core.exceptions import AlreadyExist, DoesNotExist

from .query_maker import BaseSQLQueryMaker

COLUMN_NAME = str
COLUMN_VALUE = str


class BasePSQLRepo:

    _query_maker: BaseSQLQueryMaker = BaseSQLQueryMaker()
    _connection: Connection

    def __init__(
            self,
            *,
            connection: Connection,
    ) -> None:
        self._connection = connection

    async def get_by_pk(
            self,
            *,
            table: DBTables,
            pk: int
    ) -> dict[str, str]:
        query: str = self._query_maker.get_by_pk(table)
        record: dict[str, str] = await self._connection.fetchrow(query, pk)

        if not record:
            raise DoesNotExist()
        return record

    async def get_by_where(
            self,
            *,
            table: DBTables,
            where: str,
            values: tuple[str | int, ...] = tuple(),
    ) -> dict[str, str]:
        query: str = self._query_maker.get_by_where(table=table, where=where)
        record: dict[str, str] = await self._connection.fetchrow(query, *values)

        if not record:
            raise DoesNotExist()
        return record

    async def get_list(
            self,
            *,
            table: DBTables,
            where: str = '',
            limit: int | None = None,
            offset: int | None = None,
            order_by: tuple[str, ...] = tuple(),
            values: tuple[str | int, ...] = tuple(),
    ) -> list[dict[str, str]]:
        query: str = self._query_maker.get_list(
            table=table,
            where=where,
            limit=limit,
            offset=offset,
            order_by=order_by
        )
        return await self._connection.fetch(query, *values)

    async def get_total(
            self,
            *,
            table: DBTables,
            where: str = '',
            values: tuple[str | int, ...] = tuple(),
    ) -> int:
        query: str = self._query_maker.get_total(table=table, where=where)
        record: dict[str, str] = await self._connection.fetchrow(query, *values)
        return int(record['total'])

    async def create(
            self,
            *,
            table: DBTables,
            data: dict[COLUMN_NAME, COLUMN_VALUE]
    ) -> dict[str, str]:
        keys: tuple[str | int, ...] = tuple(data.keys())
        values: tuple[str | int, ...] = tuple(data.values())
        query: str = self._query_maker.create(table=table, data_keys=keys)
        try:
            record: dict[str, str] = \
                await self._connection.fetchrow(query, *values)
        except UniqueViolationError:
            raise AlreadyExist()
        return record

    async def update_by_pk(
            self,
            *,
            table: DBTables,
            pk: int,
            data_for_update: dict[COLUMN_NAME, COLUMN_VALUE]
    ) -> dict[str, str]:
        keys: tuple[str | int, ...] = tuple(data_for_update.keys())
        values: tuple[str | int, ...] = tuple(data_for_update.values())
        query: str = self._query_maker.update_by_pk(table=table, data_keys=keys)
        record: dict[str, str] = \
            await self._connection.fetchrow(query, *(*values, pk))

        if not record:
            raise DoesNotExist()
        return record

    async def delete_by_pk(
            self,
            *,
            table: DBTables,
            pk: int
    ) -> None:
        query: str = self._query_maker.delete_by_pk(table)
        await self._connection.execute(query, pk)
