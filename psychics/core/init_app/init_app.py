from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from psychics.core.loggers import root_logger
from psychics.routers.http import main_router

from .scripts import EventHandlersFactory

logger = root_logger


class AppFactory:

    @staticmethod
    def get_app() -> FastAPI:
        logger.info('app setup is started')
        app = FastAPI()

        origins: list[str] = [
            'http://0.0.0.0:3000'
        ]

        app.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

        app.add_event_handler(
            'startup',
            EventHandlersFactory.startup()
        )

        app.add_event_handler(
            'startup',
            EventHandlersFactory.create_psychics()
        )

        app.add_event_handler(
            'shutdown',
            EventHandlersFactory.shutdown()
        )
        app.include_router(main_router)
        logger.info('app setup is finished')
        return app
