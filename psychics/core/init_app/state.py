from abc import ABC, abstractmethod

from psychics.core.config import IConfig
from psychics.core.config.factories import ConfigFactory
from psychics.core.loggers import root_logger

logger = root_logger


class IState(ABC):

    @property
    @abstractmethod
    def config(self) -> IConfig:
        pass


class State(IState):
    """Must be created through StateFactory"""

    _config: IConfig

    def __init__(
            self,
            *,
            config: IConfig,
    ) -> None:
        self._config = config

    @property
    def config(self) -> IConfig:
        return self._config


class StateFactory:
    """Can be call from everywhere. Uses Singleton pattern"""
    _state: State

    @classmethod
    async def _create_state(cls) -> State:
        config: IConfig = ConfigFactory.get_config()
        return State(
            config=config,
        )

    @classmethod
    async def get_state(cls) -> State:
        if not hasattr(cls, '_state'):
            cls._state = await cls._create_state()
            logger.info('State was created')
        return cls._state
