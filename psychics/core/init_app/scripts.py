from typing import Any, Callable, Coroutine

from psychics.core.loggers import root_logger
from psychics.psychics.dtos import PsychicCreationForm
from psychics.psychics.exceptions import PsychicAlreadyExist
from psychics.psychics.repos import IPsychicRepo, PsychicRepoInternal
from psychics.psychics.use_cases import CreatePsychicUseCase

from .state import StateFactory

logger = root_logger


class EventHandlersFactory:
    """class for creating event handlers for fastapi"""

    @staticmethod
    def startup() -> Callable[[], Coroutine[Any, Any, None]]:
        async def _startup() -> None:
            await StateFactory.get_state()
            logger.info('Startup is finished')
        return _startup

    @staticmethod
    def create_psychics() -> Callable[[], Coroutine[Any, Any, None]]:
        async def _create_psychics() -> None:
            forms: list[PsychicCreationForm] = [
                PsychicCreationForm(name='Паша'),
                PsychicCreationForm(name='Саша'),
                PsychicCreationForm(name='Гриша'),
            ]
            for form in forms:
                repo: IPsychicRepo = PsychicRepoInternal()
                use_case: CreatePsychicUseCase = CreatePsychicUseCase(
                    repo=repo,
                    psychic_form=form
                )
                try:
                    await use_case.execute()
                    logger.info('Psychic created successfully')
                except PsychicAlreadyExist:
                    pass
        return _create_psychics

    @staticmethod
    def shutdown() -> Callable[[], Coroutine[Any, Any, None]]:
        async def _shutdown() -> None:
            pass
        return _shutdown
