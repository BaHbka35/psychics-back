

class DoesNotExist(Exception):

    def __init__(self) -> None:
        self.message = 'Object does not exist'


class AlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'Object already exists'
