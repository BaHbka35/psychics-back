from __future__ import annotations

from enum import Enum
from typing import Callable

import loguru
from loguru import logger

LOGS_DIR: str = '/code/logs'

LOGGER_FORMAT = \
    "{time:DD-MM-YYYY at HH:mm:ss} [<level>{level: <8}</level>] Message : {message}" # noqa


class LoggersNames(str, Enum):
    root: str = 'root'


class LoggersFiles(str, Enum):
    root: str = f'{LOGS_DIR}/root.log'

    def __str__(self) -> str:
        return self.value


class LoggersRotations(str, Enum):
    root: str = '00:00'

    def __str__(self) -> str:
        return self.value


def make_filter(name: str) -> Callable[[loguru.Record], bool]:
    def func(record: loguru.Record) -> bool:
        return record["extra"].get("name") == name
    return func


logger.add(
    LoggersFiles.root,
    filter=make_filter(LoggersNames.root),
    format=LOGGER_FORMAT,
    rotation=LoggersRotations.root,
    compression="zip"
)

root_logger: loguru.Logger = logger.bind(name=LoggersNames.root)
