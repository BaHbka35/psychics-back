from dataclasses import asdict, dataclass


@dataclass
class BaseDTO:

    to_dict = asdict
