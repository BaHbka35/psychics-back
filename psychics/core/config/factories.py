from __future__ import annotations

from os import environ

from psychics.core.config.configs import (
    Config,
)
from psychics.core.loggers import root_logger

logger = root_logger


class ConfigFactory:

    _config_instance: Config

    @classmethod
    def _setup_config(cls) -> None:
        logger.info('config setup is started')
        debug: bool = bool(environ['DEBUG'])
        cls._config_instance = Config(
            debug=debug,
        )
        logger.info('config setup is finished')

    @classmethod
    def get_config(cls) -> Config:
        if not hasattr(cls, '_config_instance'):
            cls._setup_config()
        return cls._config_instance
