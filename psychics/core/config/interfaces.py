from abc import ABC
from dataclasses import dataclass


@dataclass(frozen=True, kw_only=True)
class IConfig(ABC):
    debug: bool

    def __post_init__(self) -> None:
        if self.__class__ == IConfig:
            raise TypeError("Cannot instantiate abstract class.")
