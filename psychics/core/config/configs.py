from __future__ import annotations

from dataclasses import dataclass

from .interfaces import IConfig


@dataclass(frozen=True, kw_only=True)
class Config(IConfig):
    """Should not be created directly. Only through factory"""


