from abc import ABC, abstractmethod

from psychics.psychics.dtos import Psychic, PsychicCreationForm


class IPsychicRepo(ABC):

    @abstractmethod
    async def create(self, form: PsychicCreationForm) -> Psychic:
        pass

    @abstractmethod
    async def get_all(self) -> list[Psychic]:
        pass

    @abstractmethod
    async def get_by_id(self, id: str) -> Psychic:
        pass

    @abstractmethod
    async def update(self, psychic: Psychic) -> Psychic:
        pass