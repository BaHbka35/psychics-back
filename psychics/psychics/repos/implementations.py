from uuid import uuid4

from psychics.data.psychics import psychics
from psychics.psychics.dtos import Psychic, PsychicCreationForm
from psychics.psychics.exceptions import PsychicNotExist

from .interfaces import IPsychicRepo


class PsychicRepoInternal(IPsychicRepo):

    async def create(self, form: PsychicCreationForm) -> Psychic:
        id: str = str(uuid4())
        psychics[id] = Psychic(**form.to_dict(), id=id, rating=0)
        return psychics[id]

    async def get_all(self) -> list[Psychic]:
        return list(psychics.values())

    async def get_by_id(self, id: str) -> Psychic:
        result: Psychic = psychics.get(id)
        if not result:
            raise PsychicNotExist
        return result

    async def update(self, psychic: Psychic) -> Psychic:
        if not psychics.get(psychic.id):
            raise PsychicNotExist
        psychics[psychic.id] = psychic
        return psychics[psychic.id]


