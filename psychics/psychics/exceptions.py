

class PsychicAlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'Psychic already exists'


class PsychicNotExist(Exception):

    def __init__(self) -> None:
        self.message = 'PsychicNotExist does not exist'
