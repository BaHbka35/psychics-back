from psychics.psychics.dtos import Psychic, PsychicCreationForm
from psychics.psychics.exceptions import PsychicAlreadyExist
from psychics.psychics.repos import IPsychicRepo


class CreatePsychicUseCase:

    _repo: IPsychicRepo
    _form: PsychicCreationForm

    def __init__(
            self,
            *,
            repo: IPsychicRepo,
            psychic_form: PsychicCreationForm
    ) -> None:
        self._repo = repo
        self._form = psychic_form

    async def execute(self) -> Psychic:
        try:
            return await self._repo.create(self._form)
        except PsychicAlreadyExist:
            raise


