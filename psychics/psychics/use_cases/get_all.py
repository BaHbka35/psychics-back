from psychics.psychics.dtos import Psychic
from psychics.psychics.repos import IPsychicRepo


class GetAllPsychicsUseCase:

    _repo: IPsychicRepo

    def __init__(self, *, repo: IPsychicRepo) -> None:
        self._repo = repo

    async def execute(self) -> list[Psychic]:
        return await self._repo.get_all()