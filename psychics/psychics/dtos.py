from dataclasses import dataclass
from random import randint

from psychics.core.base_dto import BaseDTO


@dataclass
class BasePsychic(BaseDTO):
    name: str

    def __post_init__(self) -> None:
        if len(self.name) > 10:
            raise TypeError


@dataclass
class PsychicCreationForm(BasePsychic):
    pass


@dataclass
class Psychic(BasePsychic):
    id: str
    rating: int

    @staticmethod
    def guess_number() -> int:
        return randint(10, 99)

