from pydantic import BaseModel


class HiddenNumberSc(BaseModel):
    id: int
    round_id: int
    number: int