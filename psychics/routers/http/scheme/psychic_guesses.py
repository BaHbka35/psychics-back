from pydantic import BaseModel

from .psychics import PsychicSc


class PsychicGuessListItem(BaseModel):
    id: int
    round_id: int
    number: int


class PsychicGuessSc(BaseModel):
    id: int
    round_id: int
    number: int
    psychic: PsychicSc
