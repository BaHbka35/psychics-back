from pydantic import BaseModel


class PsychicSc(BaseModel):
    id: int
    name: str
    rating: int
