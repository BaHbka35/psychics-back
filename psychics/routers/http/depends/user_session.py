from fastapi import HTTPException, Request


def get_user_session(request: Request) -> str:
    session = request.cookies.get('session')
    if not session:
        raise HTTPException(
            status_code=403, detail='User session was not provided')
    return session