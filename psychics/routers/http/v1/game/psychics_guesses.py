from typing import Annotated, Any

from fastapi import APIRouter, Depends, Query, Response

from psychics.game.exceptions import UserHiddenNumberAlreadyExist
from psychics.game.repos import (
    IPsychicGuessRepo,
    IRoundRepo,
    IUserHiddenNumbersRepo,
    PsychicGuessRepoInternal,
    RoundRepoInternal,
    UserHiddenNumberRepoInternal,
)
from psychics.game.use_cases import (
    CheckPsychicsGuessesUseCase,
    GetPsychicGuessesHistoryUseCase,
)
from psychics.psychics.repos import IPsychicRepo, PsychicRepoInternal
from psychics.routers.http.depends import (
    get_user_session,
)
from psychics.routers.http.scheme.psychic_guesses import PsychicGuessListItem
from psychics.routers.http.scheme.psychics import PsychicSc
from psychics.users.dtos import User

router = APIRouter(
    prefix='/psychics_guesses'
)


@router.post(
    '/check',
    status_code=200,
    description='Take number from user and compare with psychics guesses',
    responses={
        200: {'model': list[PsychicSc]}
    }
)
async def check(
        response: Response,
        user_hidden_number: Annotated[int, Query(gt=9, lt=100)] = ...,
        session: str = Depends(get_user_session)
) -> Any:
    user: User = User(session=session)
    round_repo: IRoundRepo = RoundRepoInternal()
    psychic_guess_repo: IPsychicGuessRepo = PsychicGuessRepoInternal()
    hidden_number_repo: IUserHiddenNumbersRepo = UserHiddenNumberRepoInternal()
    psychic_repo: IPsychicRepo = PsychicRepoInternal()

    case: CheckPsychicsGuessesUseCase = CheckPsychicsGuessesUseCase(
        user=user,
        user_hidden_number=user_hidden_number,
        round_repo=round_repo,
        psychic_repo=psychic_repo,
        hidden_number_repo=hidden_number_repo,
        psychic_guess_repo=psychic_guess_repo

    )
    try:
        return await case.execute()
    except UserHiddenNumberAlreadyExist:
        response.status_code = 409
        return {'message': 'Hidden number for round already exist'}


@router.get(
    '/history',
    status_code=200,
    description='returns guesses history of specific psychic for user',
    responses={
        200: {'model': list[PsychicGuessListItem]}
    }
)
async def get_history(
        psychic_id: str,
        session: str = Depends(get_user_session)
) -> Any:
    user: User = User(session=session)
    repo: IPsychicGuessRepo = PsychicGuessRepoInternal()
    case: GetPsychicGuessesHistoryUseCase = GetPsychicGuessesHistoryUseCase(
        psychic_id=psychic_id,
        repo=repo,
        user=user
    )
    return (i.to_dict() for i in await case.execute())

