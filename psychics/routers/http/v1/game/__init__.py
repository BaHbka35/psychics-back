from fastapi import APIRouter

from .hidden_numbers import router as hidden_numbers_router
from .psychics_guesses import router as psychics_guesses_router
from .round import router as round_router

router = APIRouter(
    prefix='/game'
)

router.include_router(round_router)
router.include_router(psychics_guesses_router)
router.include_router(hidden_numbers_router)
