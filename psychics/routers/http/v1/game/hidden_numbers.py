from typing import Any

from fastapi import APIRouter, Depends

from psychics.game.dtos.hidden_numbers import UserHiddenNumber
from psychics.game.repos import (
    IUserHiddenNumbersRepo,
    UserHiddenNumberRepoInternal,
)
from psychics.game.use_cases import GetHiddenNumbersHistoryUseCase
from psychics.routers.http.depends import (
    get_user_session,
)
from psychics.routers.http.scheme.hidden_numbers import HiddenNumberSc
from psychics.users.dtos import User

router = APIRouter(
    prefix='/hidden_numbers'
)


@router.get(
    '/history',
    description='Returns all hidden user number',
    responses={
        200: {'model': list[HiddenNumberSc]}
    }
)
async def get_history(
        limit: int | None = None,
        offset: int | None = None,
        session: str = Depends(get_user_session)
) -> Any:
    user: User = User(session=session)
    repo: IUserHiddenNumbersRepo = UserHiddenNumberRepoInternal()
    case: GetHiddenNumbersHistoryUseCase = GetHiddenNumbersHistoryUseCase(
        user=user,
        repo=repo,
        limit=limit,
        offset=offset
    )
    result: list[UserHiddenNumber] = await case.execute()
    return (i.to_dict() for i in result)




