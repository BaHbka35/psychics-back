from typing import Any

from fastapi import APIRouter, Depends

from psychics.game.dtos.psychic_guess import PsychicGuessOutput
from psychics.game.repos import (
    IPsychicGuessRepo,
    IRoundRepo,
    PsychicGuessRepoInternal,
    RoundRepoInternal,
)
from psychics.game.use_cases import CreateRoundUseCase
from psychics.psychics.repos import IPsychicRepo, PsychicRepoInternal
from psychics.routers.http.depends import (
    get_user_session,
)
from psychics.routers.http.scheme.psychic_guesses import PsychicGuessSc
from psychics.users.dtos import User

router = APIRouter(
    prefix='/rounds'
)


@router.post(
    '/',
    status_code=201,
    description='Create round. Returns psychic guesses.',
    responses={
        201: {'model': list[PsychicGuessSc]}
    }
)
async def create_round(
        session: str = Depends(get_user_session)
) -> Any:
    user: User = User(session=session)
    round_repo: IRoundRepo = RoundRepoInternal()
    psychic_guess_repo: IPsychicGuessRepo = PsychicGuessRepoInternal()
    psychic_repo: IPsychicRepo = PsychicRepoInternal()

    case: CreateRoundUseCase = CreateRoundUseCase(
        user=user,
        round_repo=round_repo,
        psychic_repo=psychic_repo,
        psychic_guess_repo=psychic_guess_repo
    )

    result: list[PsychicGuessOutput] = await case.execute()
    return (i.to_dict() for i in result)


