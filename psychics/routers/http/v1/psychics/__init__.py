from fastapi import APIRouter

from .psychics import router as psychic_router

router = APIRouter(
    prefix='/psychics',
)

router.include_router(psychic_router)
