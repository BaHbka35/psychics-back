from typing import Any

from fastapi import APIRouter, Depends

from psychics.psychics.repos import IPsychicRepo, PsychicRepoInternal
from psychics.psychics.use_cases import GetAllPsychicsUseCase
from psychics.routers.http.depends import (
    get_user_session,
)
from psychics.routers.http.scheme.psychics import PsychicSc

router = APIRouter(
    prefix=''
)


@router.get(
    '/',
    status_code=200,
    description='Returns all psychics',
    responses={
        200: {'model': list[PsychicSc]}
    }
)
async def get_all(
        session: str = Depends(get_user_session)
) -> Any:
    repo: IPsychicRepo = PsychicRepoInternal()
    case: GetAllPsychicsUseCase = GetAllPsychicsUseCase(repo=repo)
    return (i.to_dict() for i in await case.execute())
