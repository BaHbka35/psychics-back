from typing import Any

from fastapi import APIRouter, Depends, Response

from psychics.routers.http.depends import (
    get_user_session,
)
from psychics.routers.http.scheme.users import UserSc
from psychics.users.dtos import User
from psychics.users.exceptions import UserAlreadyExist, UserDoesNotExist
from psychics.users.repos import IUserRepo, UserRepoInternal
from psychics.users.use_cases import CreateUserUseCase, GetUserBySessionUseCase

router = APIRouter(
    prefix=''
)


@router.post(
    '/',
    status_code=201,
    description='API for creating user session',
    responses={
        201: {'model': UserSc},
    }
)
async def create_user(
        response: Response,
) -> Any:
    repo: IUserRepo = UserRepoInternal()
    case: CreateUserUseCase = CreateUserUseCase(repo=repo)
    try:
        result: User = await case.execute()
    except UserAlreadyExist:
        response.status_code = 409
        return {'message': 'Try again'}
    response.set_cookie(
        key='session',
        value=result.session,
        httponly=True,
    )
    return UserSc(**result.to_dict())


@router.get(
    '/',
    status_code=200,
    description='get user by session',
    responses={
        200: {'model': UserSc},
        404: {'model': dict[str, str]}
    }
)
async def get_user(
        response: Response,
        session: str = Depends(get_user_session)
) -> Any:
    repo: IUserRepo = UserRepoInternal()
    case: GetUserBySessionUseCase = GetUserBySessionUseCase(
        repo=repo,
        session=session
    )
    try:
        result: User = await case.execute()
    except UserDoesNotExist:
        response.status_code = 404
        return {'message': 'User with given session is not exist'}
    return UserSc(**result.to_dict())
