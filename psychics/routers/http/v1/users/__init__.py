from fastapi import APIRouter

from .users import router as user_router

router = APIRouter(
    prefix='/users'
)

router.include_router(user_router)
