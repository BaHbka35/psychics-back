from fastapi import APIRouter

from .game import router as game_router
from .psychics import router as psychics_router
from .users import router as user_router

router = APIRouter(
    prefix='/v1'
)

router.include_router(user_router)
router.include_router(game_router)
router.include_router(psychics_router)
