from psychics.game.dtos.hidden_numbers import UserHiddenNumberCreationForm
from psychics.game.dtos.psychic_guess import PsychicGuess
from psychics.game.dtos.round import Round
from psychics.game.repos import (
    IPsychicGuessRepo,
    IRoundRepo,
    IUserHiddenNumbersRepo,
)
from psychics.psychics.dtos import Psychic
from psychics.psychics.repos import IPsychicRepo
from psychics.users.dtos import User


class CheckPsychicsGuessesUseCase:

    _user: User
    _user_hidden_number: int
    _hidden_number_repo: IUserHiddenNumbersRepo
    _round_repo: IRoundRepo
    _psychic_guess_repo: IPsychicGuessRepo
    _psychic_repo: IPsychicRepo

    def __init__(
            self,
            *,
            user: User,
            user_hidden_number: int,
            hidden_number_repo: IUserHiddenNumbersRepo,
            round_repo: IRoundRepo,
            psychic_guess_repo: IPsychicGuessRepo,
            psychic_repo: IPsychicRepo
    ) -> None:
        self._user = user
        self._user_hidden_number = user_hidden_number
        self._hidden_number_repo = hidden_number_repo
        self._round_repo = round_repo
        self._psychic_guess_repo = psychic_guess_repo
        self._psychic_repo = psychic_repo

    async def execute(self) -> list[Psychic]:
        last_round: Round = await self._round_repo.get_last_belongs_to_user(
            user=self._user)
        await self._hidden_number_repo.create(
            form=UserHiddenNumberCreationForm(
                round_id=last_round.id,
                number=self._user_hidden_number
            )
        )
        psychics_guesses: list[PsychicGuess] = \
            await self._psychic_guess_repo.get_all_by_round_id(last_round.id)

        psychics_ids_for_increase_rating: list[str] = []
        psychics_ids_for_decrease_rating: list[str] = []

        for psychic_guess in psychics_guesses:
            if psychic_guess.number != self._user_hidden_number:
                psychics_ids_for_decrease_rating.append(psychic_guess.psychic_id) # noqa
                continue
            psychics_ids_for_increase_rating.append(psychic_guess.psychic_id)

        psychics_output: list[Psychic] = []
        for psychic_id in psychics_ids_for_increase_rating:
            psychic: Psychic = await self._psychic_repo.get_by_id(psychic_id)
            psychic.rating += 1
            psychics_output.append(psychic)
            await self._psychic_repo.update(psychic)

        for psychic_id in psychics_ids_for_decrease_rating:
            psychic: Psychic = await self._psychic_repo.get_by_id(psychic_id)
            psychic.rating -= 1
            await self._psychic_repo.update(psychic)

        return psychics_output


