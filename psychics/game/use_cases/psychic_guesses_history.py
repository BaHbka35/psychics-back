from psychics.game.dtos.psychic_guess import PsychicGuess
from psychics.game.repos import IPsychicGuessRepo
from psychics.users.dtos import User


class GetPsychicGuessesHistoryUseCase:

    _user: User
    _psychic_id: str
    _repo: IPsychicGuessRepo

    def __init__(
            self,
            *,
            repo: IPsychicGuessRepo,
            user: User,
            psychic_id: str
    ) -> None:
        self._psychic_id = psychic_id
        self._user = user
        self._repo = repo

    async def execute(self) -> list[PsychicGuess]:
        return await self._repo.get_all_by_psychic_id_belongs_to_user_session(
            psychic_id=self._psychic_id,
            user_session=self._user.session
        )