from psychics.game.dtos.psychic_guess import (
    PsychicGuessCreationForm,
    PsychicGuessOutput,
)
from psychics.game.dtos.round import Round, RoundCreationForm
from psychics.game.repos import IPsychicGuessRepo, IRoundRepo
from psychics.psychics.dtos import Psychic
from psychics.psychics.repos import IPsychicRepo
from psychics.users.dtos import User


class CreateRoundUseCase:

    _round_repo: IRoundRepo
    _psychic_guess_repo: IPsychicGuessRepo
    _user: User
    _psychic_repo: IPsychicRepo

    def __init__(
            self,
            *,
            user: User,
            round_repo: IRoundRepo,
            psychic_guess_repo: IPsychicGuessRepo,
            psychic_repo: IPsychicRepo,
    ) -> None:
        self._round_repo = round_repo
        self._psychic_guess_repo = psychic_guess_repo
        self._user = user
        self._psychic_repo = psychic_repo

    async def execute(self) -> list[PsychicGuessOutput]:
        round_form: RoundCreationForm = RoundCreationForm(
            user_session=self._user.session)
        round: Round = await self._round_repo.create(round_form)
        psychics: list[Psychic] = await self._psychic_repo.get_all()
        psychics_guesses_output: list[PsychicGuessOutput] = []
        for psychic in psychics:
            guess_number: int = psychic.guess_number()
            psychic_guess_form: PsychicGuessCreationForm = \
                PsychicGuessCreationForm(
                    psychic_id=psychic.id,
                    round_id=round.id,
                    number=guess_number
                )
            psychic_guess = await self._psychic_guess_repo.create(
                psychic_guess_form)
            psychics_guesses_output.append(
                PsychicGuessOutput(
                    id=psychic_guess.id,
                    round_id=psychic_guess.round_id,
                    number=psychic_guess.number,
                    psychic=psychic,
                )
            )
        return psychics_guesses_output








