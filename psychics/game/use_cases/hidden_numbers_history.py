from psychics.game.dtos.hidden_numbers import UserHiddenNumber
from psychics.game.repos import IUserHiddenNumbersRepo
from psychics.users.dtos import User


class GetHiddenNumbersHistoryUseCase:

    _user: User
    _repo: IUserHiddenNumbersRepo
    _limit: int | None
    _offset: int | None

    def __init__(
            self,
            *,
            user: User,
            repo: IUserHiddenNumbersRepo,
            limit: int | None = None,
            offset: int | None = None,
    ) -> None:
        self._repo = repo
        self._user = user
        self._limit = limit
        self._offset = offset

    async def execute(self) -> list[UserHiddenNumber]:
        return await self._repo.get_all_by_user_session(
            session=self._user.session,
            limit=self._limit,
            offset=self._offset
        )