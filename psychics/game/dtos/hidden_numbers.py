from dataclasses import dataclass

from psychics.core.base_dto import BaseDTO


@dataclass
class UserHiddenNumberBase(BaseDTO):
    round_id: str
    number: int


@dataclass
class UserHiddenNumberCreationForm(UserHiddenNumberBase):
    pass


@dataclass
class UserHiddenNumber(UserHiddenNumberBase):
    id: str