from dataclasses import dataclass

from psychics.core.base_dto import BaseDTO


@dataclass
class RoundBase(BaseDTO):
    user_session: str


@dataclass
class RoundCreationForm(RoundBase):
    pass


@dataclass
class Round(RoundBase):
    id: str
