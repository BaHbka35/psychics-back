from dataclasses import dataclass

from psychics.core.base_dto import BaseDTO
from psychics.psychics.dtos import Psychic


@dataclass
class PsychicGuessBase(BaseDTO):
    psychic_id: str
    round_id: str
    number: int


@dataclass
class PsychicGuessCreationForm(PsychicGuessBase):
    pass


@dataclass
class PsychicGuess(PsychicGuessBase):
    id: str


@dataclass
class PsychicGuessOutput(BaseDTO):
    id: str
    round_id: str
    number: int
    psychic: Psychic
