

class RoundAlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'Round already exists'


class RoundDoesNotExist(Exception):

    def __init__(self) -> None:
        self.message = 'Round does not exist'


class PsychicGuessAlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'Psychic guess already exists'


class UserHiddenNumberAlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'User hidden number already exists'
