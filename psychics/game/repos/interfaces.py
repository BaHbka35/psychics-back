from abc import ABC, abstractmethod

from psychics.game.dtos.hidden_numbers import (
    UserHiddenNumber,
    UserHiddenNumberCreationForm,
)
from psychics.game.dtos.psychic_guess import (
    PsychicGuess,
    PsychicGuessCreationForm,
)
from psychics.game.dtos.round import Round, RoundCreationForm
from psychics.users.dtos import User


class IRoundRepo(ABC):

    @abstractmethod
    async def create(self, form: RoundCreationForm) -> Round:
        pass

    @abstractmethod
    async def get_last_belongs_to_user(self, user: User) -> Round:
        pass


class IPsychicGuessRepo(ABC):

    @abstractmethod
    async def create(self, form: PsychicGuessCreationForm) -> PsychicGuess:
        pass

    @abstractmethod
    async def get_all_by_round_id(self, round_id: str) -> list[PsychicGuess]:
        pass

    @abstractmethod
    async def get_all_by_psychic_id_belongs_to_user_session(
            self,
            psychic_id: str,
            user_session: str,
    ) -> list[PsychicGuess]:
        pass


class IUserHiddenNumbersRepo(ABC):

    @abstractmethod
    async def create(
            self,
            form: UserHiddenNumberCreationForm
    ) -> UserHiddenNumber:
        pass

    @abstractmethod
    async def get_all_by_user_session(
            self,
            *,
            session: str,
            limit: int | None,
            offset: int | None
    ) -> list[UserHiddenNumber]:
        pass
