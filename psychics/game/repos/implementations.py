from uuid import uuid4

from psychics.data.hidden_numbers import hidden_numbers
from psychics.data.psychic_guesses import (
    psychics_guesses_by_psychic_id,
    psychics_guesses_by_round,
)
from psychics.data.rounds import rounds
from psychics.game.dtos.hidden_numbers import (
    UserHiddenNumber,
    UserHiddenNumberCreationForm,
)
from psychics.game.dtos.psychic_guess import (
    PsychicGuess,
    PsychicGuessCreationForm,
)
from psychics.game.dtos.round import Round, RoundCreationForm
from psychics.game.exceptions import (
    RoundDoesNotExist,
)
from psychics.users.dtos import User

from .interfaces import IPsychicGuessRepo, IRoundRepo, IUserHiddenNumbersRepo


class RoundRepoInternal(IRoundRepo):

    async def create(self, form: RoundCreationForm) -> Round:
        id: str = str(uuid4())
        round: Round = Round(**form.to_dict(), id=id)
        if not rounds.get(form.user_session):
            rounds[form.user_session] = []
        rounds[form.user_session].append(round)
        return round

    async def get_last_belongs_to_user(self, user: User) -> Round:
        if not rounds.get(user.session):
            raise RoundDoesNotExist()
        return rounds[user.session][-1]


class PsychicGuessRepoInternal(IPsychicGuessRepo):

    async def create(self, form: PsychicGuessCreationForm) -> PsychicGuess:
        id: str = str(uuid4())
        psychic_guess: PsychicGuess = PsychicGuess(**form.to_dict(), id=id)
        if not psychics_guesses_by_round.get(psychic_guess.round_id):
            psychics_guesses_by_round[psychic_guess.round_id] = []
        if not psychics_guesses_by_psychic_id.get(psychic_guess.psychic_id):
            psychics_guesses_by_psychic_id[psychic_guess.psychic_id] = []
        psychics_guesses_by_psychic_id[psychic_guess.psychic_id].append(
            psychic_guess)
        psychics_guesses_by_round[psychic_guess.round_id].append(psychic_guess)
        return psychic_guess

    async def get_all_by_round_id(self, round_id: str) -> list[PsychicGuess]:
        return psychics_guesses_by_round.get(round_id, [])

    async def get_all_by_psychic_id_belongs_to_user_session(
            self,
            psychic_id: str,
            user_session: str,
    ) -> list[PsychicGuess]:
        user_rounds: list[Round] = rounds.get(user_session, [])
        psychic_guesses: list[PsychicGuess] = []
        for round in user_rounds:
            round_guesses: list[PsychicGuess] = psychics_guesses_by_round.get(
                round.id)
            for i in round_guesses:
                if i.psychic_id == psychic_id:
                    psychic_guesses.append(i)
        return psychic_guesses


class UserHiddenNumberRepoInternal(IUserHiddenNumbersRepo):

    async def create(
            self,
            form: UserHiddenNumberCreationForm
    ) -> UserHiddenNumber:
        id: str = str(uuid4())
        hidden_number: UserHiddenNumber = UserHiddenNumber(
            **form.to_dict(), id=id)
        hidden_numbers[hidden_number.round_id] = hidden_number
        return hidden_number

    async def get_all_by_user_session(
            self,
            *,
            session: str,
            limit: int | None,
            offset: int | None
    ) -> list[UserHiddenNumber]:
        user_rounds: list[Round] = rounds.get(session, [])
        user_hidden_numbers: list[UserHiddenNumber] = []
        for round in user_rounds:
            if not hidden_numbers.get(round.id):
                continue
            user_hidden_numbers.append(hidden_numbers.get(round.id))
        offset = 0 if offset == 1 or offset is None else offset
        if not limit:
            return user_hidden_numbers[offset:]
        return user_hidden_numbers[offset: offset + limit]
