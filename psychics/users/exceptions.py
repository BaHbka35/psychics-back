

class UserAlreadyExist(Exception):

    def __init__(self) -> None:
        self.message = 'User already exists'


class UserDoesNotExist(Exception):

    def __init__(self) -> None:
        self.message = 'User does not exist'
