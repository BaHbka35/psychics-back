from psychics.data.users import users
from psychics.users.dtos import User, UserCreationFrom
from psychics.users.exceptions import UserAlreadyExist, UserDoesNotExist

from .interfaces import IUserRepo


class UserRepoInternal(IUserRepo):

    async def create(self, form: UserCreationFrom) -> User:
        if users.get(form.session):
            raise UserAlreadyExist()
        users[form.session] = User(**form.to_dict())
        return users[form.session]

    async def get_by_session(self, session: str) -> User:
        if not users.get(session):
            raise UserDoesNotExist
        return users.get(session)
