from abc import ABC, abstractmethod

from psychics.users.dtos import User, UserCreationFrom


class IUserRepo(ABC):

    @abstractmethod
    async def create(self, form: UserCreationFrom) -> User:
        pass

    @abstractmethod
    async def get_by_session(self, session: str) -> User:
        pass