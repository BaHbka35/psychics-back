from base64 import b64encode
from uuid import uuid4


class UserSessionGenerator:

    def generate(self) -> str:
        uuid_str: str = str(uuid4())
        in_bytes: bytes = uuid_str.encode()
        session_in_bytes: bytes = b64encode(in_bytes)
        session: str = session_in_bytes.decode()
        return session
