from psychics.users.dtos import User, UserCreationFrom
from psychics.users.exceptions import UserAlreadyExist
from psychics.users.repos import IUserRepo
from psychics.users.session_generator import UserSessionGenerator


class CreateUserUseCase:

    _repo: IUserRepo
    _session_generator: UserSessionGenerator = UserSessionGenerator()

    def __init__(
            self,
            *,
            repo: IUserRepo
    ) -> None:
        self._repo = repo

    async def execute(self) -> User:
        form: UserCreationFrom = UserCreationFrom(
            session=self._session_generator.generate()
        )
        try:
            return await self._repo.create(form)
        except UserAlreadyExist:
            raise
