from psychics.users.dtos import User
from psychics.users.exceptions import UserDoesNotExist
from psychics.users.repos import IUserRepo


class GetUserBySessionUseCase:

    _repo: IUserRepo
    _session: str

    def __init__(
            self,
            *,
            repo: IUserRepo,
            session: str
    ) -> None:
        self._repo = repo
        self._session = session

    async def execute(self) -> User:
        try:
            return await self._repo.get_by_session(self._session)
        except UserDoesNotExist:
            raise
