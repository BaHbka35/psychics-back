from dataclasses import dataclass

from psychics.core.base_dto import BaseDTO


@dataclass
class User(BaseDTO):
    session: str

    def __post_init__(self) -> None:
        if len(self.session) > 50:
            raise TypeError


@dataclass
class UserCreationFrom(User):
    pass
